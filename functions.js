"use strict";

import * as crypto from 'crypto';
import _ from 'lodash';
import { promises as fs } from 'fs';
//throw new Error('Not yet implemented');

// internal function used to indicate that a function has not been implemented
//
async function THROW_UNIMPLEMENTED () {
	throw new Error(`Not yet implemented`);
}

export const inRange = max => min => x => x >= min && x <= max;

export function top ( a ) {
	if (! Array.isArray(  a  ) )
		throw new Error (`function "top" expects an array`);
	if (  a.length === 0  ) 
		return undefined;
	return a[ a.length - 1 ];
}

export function help (self) {
	if (self === undefined)
		return `Please call help with a refernce to this module.. My Body \n\${help.toString()}`;
	return `
		----------MY FUNCTIONS---------
		${FindAllMethods(self)}
		----------THIS HELP FUNCTION---
		${help.toString()}
	`;
}

// encrypt a message with a public key so it can only be decrypted by the private key
// todo: add implementation date
export function encrypt_for(message, public_key) {
	THROW_UNIMPLEMENTED();
}

// todo: add implementation date
export function decrypt_with(message, private_key) {
	THROW_UNIMPLEMENTED();
}

/*
	Encrypt a message
// todo: add implementation date
*/
export function encrypt () {
	THROW_UNIMPLEMENTED()
}

/*

// todo: add implementation date
*/
export function decrypt () {
	THROW_UNIMPLEMENTED()
}

/*		ohash()
 *		like hash() but takes an object and stringifys it first
 *		William Doyle
 *		September 5th 2021
 * */
export function ohash(input, algo = 'SHA256') {
	return hash(JSON.stringify(input), algo);
}

/*		hash()
 *		@in [
 *			input: string
 *				The thing to hash
 *			algo: optional string
 *				the hash algorithum to use. Defaults to SHA256
 *		]
 *		@out hash of input
 *		William Doyle
 *		pre september 6th 2021
 * */
export function hash(input, algo = 'SHA256') {
	const hash = crypto.createHash(algo);
	hash.update(input).end();
	return hash.digest('hex');
}

/**
 * sign --> cryptographicallyy sign a message
 * @param {*} message 
 * @param {*} privatekey 
 * @param {*} algo 
 * @returns	signature 
 */
export function sign(message, privatekey, algo = 'SHA256' ) {
	const sign = crypto.createSign(algo);
	sign.update(message).end();
	const signature = sign.sign(privatekey);
	return signature;
}

/**
 * checkSignature --> check a signature was signed 
 * @param {} message 
 * @param {*} signerPublicKey 
 * @param {*} signature 
 * @param {*} algo 
 * @returns 
 */
export function checkSignature( message, signerPublicKey, signature, algo = 'SHA256') {
	const verifier = crypto.createVerify(algo);
	verifier.update(message);
	return verifier.verify(signerPublicKey, signature);
}

/**
 * Generate a cryptographic key pair
 * @param {} algo 
 * @param {*} modLen 
 * @param {*} publickKeyEncodingType 
 * @param {*} privateKeyEncodingType 
 * @param {*} encodingFormat 
 * @returns 
 */
export function GenerateKeys(
	algo = 'rsa',
	modLen = 2048,
	publickKeyEncodingType = 'spki', 
	privateKeyEncodingType = 'pkcs8',
	encodingFormat= 'pem',
){
	const keypair = crypto.generateKeyPairSync(algo, {
			modulusLength: modLen,
			publicKeyEncoding:  {type: publickKeyEncodingType, format: encodingFormat},
			privateKeyEncoding: {type: privateKeyEncodingType, format: encodingFormat}
		});
	return keypair;
}

export const cryptographic = {
	GenerateKeys,
	hash,
	ohash,
	sign,
	checkSignature,
	encrypt,
	decrypt,
	encrypt_for,
	decrypt_with,
};

// export cryptographic;

/**
 * Generate a random color
 * @returns 
 */
export function RandomColor() {
	function internal_recursive_RandomColor (depth_limit) {
		return function (color_string_so_far) {
			if (depth_limit > 0)
				return internal_recursive_RandomColor(depth_limit - 1)(color_string_so_far + RandomHexDigitChar() );
			return color_string_so_far;
		}
	}

	return internal_recursive_RandomColor(6)("#");
}

/**
 *	pick a value from the provided array
 */
export function RandomSelection(options) {
	if (!Array.isArray(options)) 
		throw new Error(`"options" must be an array (got "${typeof options}")`);

	return options[RandomIndexOfAnArray(options)];
}

/**
 *   does not mutate the original array
 * 	 RandomIndexOfAnArray(array)
 * */
export function RandomIndexOfAnArray(arr) {
	if (!Array.isArray(arr)) 
		throw new Error(`"arr" must be an array (got "${typeof arr}")`);

	return RandomInteger(0, arr.length);
}

/**
 * Randomly select a value from an array but use a weighted random selection
 * 
 */
export function WeightedRandomSelection(weights) {
	if (!Array.isArray(weights)) 
		throw new Error(`"weights" must be an array (got "${typeof weights}")`);
	if (weights.length === 0)
		throw new Error(`"weights" must not be empty`);
	if(weights.length !== weights.filter(x => x > 0).length)
		throw new Error(`"weights" must contain only positive numbers`);
	
	return options => {
		if(!Array.isArray(options)) 
			throw new Error(`"options" must be an array (got "${typeof options}")`);
		if (options.length !== weights.length)
			throw new Error(`"options" and "weights" must be the same length (got ${options.length} and ${weights.length})`);
		
		//const weightMap = 
		THROW_UNIMPLEMENTED();
	}
}

export function RandomNumber(min, max) {
	if ([min, max].includes(undefined))
		throw new Error(`RandomNumber:: expected two numbers (got "${typeof min}" and "${typeof max}")`);
	if (min > max)
		throw new Error(`RandomNumber:: "min" may not be larger than "max"!`);
	return (Math.random() * (max - min)) + min;
}

export function RandomInteger(min, max) {
	if (min - Math.floor(min) !== 0)
		throw new Error(`RandomInteger:: "min" must be a whole number`);
	if (max - Math.floor(max) !== 0)
		throw new Error(`RandomInteger:: "max" must be a whole number`);
	return Math.floor(RandomNumber(min, max));
}

export const RandomBool = () => RandomNumber(0, 10) > 5;
export const RandomBit = () => RandomBool() ? 1:0;

export function RandomOrder(arr) {
	return clone(arr).sort(() => (Math.random() > 0.5)? 1:-1);
}


export function clone_dropClass (obj) {
	return JSON.parse(JSON.stringify(obj));
}

export function clone(obj, __class = undefined) {

	if (__class === undefined)
		return clone_dropClass(obj);

	// TODO: use Object.assign to clone while maintianing class
	THROW_UNIMPLEMENTED();
	
}

/**
 *
 *https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze
 * */
export function deepFreeze(object) {
  // Retrieve the property names defined on object
  const propNames = Object.getOwnPropertyNames(object);

  // Freeze properties before freezing self

  for (const name of propNames) {
    const value = object[name];

    if (value && typeof value === "object") {
      deepFreeze(value);
    }
  }

  return Object.freeze(object);
}

export function df(object) {
	return deepFreeze(object);
}

/*
 *	Take an object and return an immutable copy of that object
 * */
export function cpylck(obj) {
	return deepFreeze(clone(obj));
}

export function SimpleEqual(a) {
	//return b => JSON.stringify(a) === JSON.stringify(b);
	return b => _.isEqual(a, b);
}

export async function TimeExecution(foo, args) {
	const start = new Date().getTime();
	const rval = await foo(...args)
	const end = new Date().getTime();
	console.log(`\nruntime: ${(end - start) * 0.001} seconds\n`);
	return rval;
}

//time:: Nothing -> Nothing -> Time In Ms
export function timer() {
    const start = new Date().getTime();
    return () => new Date().getTime() - start;
}


export function FindAllMethods(obj) {
	//https://www.w3resource.com/javascript-exercises/javascript-object-exercise-11.php
	return Object.getOwnPropertyNames(obj).filter(property => typeof obj[property] === "function");
}

export const sq = n => Math.pow(n, 2);	// square a number

export const delta = a => b => Math.abs(a - b);

export function CurriedVectorDistance(_a){
	const a = deepFreeze(_a);
	return function (_b) {
		const b = deepFreeze(_b);
		const primedA = {};
		const primedB = {};

		// can the following 2 code blocks be optamized?
		for (const key of Object.keys(a))
			primedA[key] = a[key];
		for (const key of Object.keys(b)) 
			primedA[key] = a[key] ?? 0;
		
		for (const key of Object.keys(b))
			primedB[key] = b[key];
		for (const key of Object.keys(a)) 
			primedB[key] = b[key] ?? 0;

		const vector_differences = {};
		for (const key of Object.keys(primedA)) 
			vector_differences[key] = primedA[key] - primedB[key];

		return Math.sqrt(Object.values(vector_differences).reduce((acc, vdif) => acc + sq(vdif), 0));
	}
}

/**
 * Uncurried function to get the displacement between two vectors
 * @param {} a 
 * @param {*} b 
 * @returns 
 */
export function VectorDistance(a, b) {
	return CurriedVectorDistance(a)(b);
}

// untested ----------  functions below this line need to be tested. Move above this line once tested

/**
 * Add all values in a numerical array
 * @param {} arr 
 * @returns 
 */
export function sum (arr) {
	return arr.reduce((acc, el) => acc + el, 0);
}

/**
 *	positveIntegersLessThanOrEqualToN()
 *	returns array of positive integers less than or equal to n
 */
export function positiveIntegersLessThanOrEqualToN(n) {
	function __positiveIntegersLessThanOrEqualToN(n , array) {
		if (n <= 0) 
			return array;
		return __positiveIntegersLessThanOrEqualToN(n - 1, array.concat([n]));
	}
	return __positiveIntegersLessThanOrEqualToN(n, []);
}

export function sumIntegersLessThanOrEqualToN (n) {
	return sum(positiveIntegersLessThanOrEqualToN(n));
}

/**
 * Extract a number from a string
 */
export function pullNumber(sn) {
	const matches = sn.match(/\d+/g);
	const n = Number(matches[0]);
	return n;
}

/**
 * Get a random hex digit as a string of length 1
 */
export function RandomHexDigitChar() {
	const number = RandomInteger(0, 16);
	return Number(number).toString(16);
}

export const node = {
	// tired of importing fs
	read: async function(fname, encoding = 'utf-8') {
		const s = await fs.readFile(fname, encoding );
		return s;
	},
	write: fname => async body => await fs.writeFile(fname, body),
	
}

export const deno = {
	read : THROW_UNIMPLEMENTED, 
	write: THROW_UNIMPLEMENTED,
}

export const zip = arra => arrb => {
    if (arra.length !== arrb.length)
        throw new Error('cannot zip arrays of different lengths');
    return arra.map((el, index) => [el, arrb[index]]);
}

export const unzip = arr => {
	// take an array of pairs
	// return a pair of arrays
	if (arr[0].length !== 2)
		throw new Error(`Unzip takes an array of arrays where each sub array is 2 elements long`);

	function rcur_unzip(arr, progressA, progressB) {
		if (arr.length === 0)
			return [progressA, progressB];
		const [head, ...tail] = arr;
		return rcur_unzip(tail, [...progressA, head[0]], [...progressB, head[1]]);
	}

	return rcur_unzip(arr, [], []);
}


/**
 * 
 * @param {*} arr 
 * @returns 
 */
export function unzipN ( arr ) {
	if (arr.length === 0)
		return;

	if (sum(arr.map(el => el.length)) !== arr[0].length * arr.length) // not a good test.. 1 might be 1 more and another 1 less @TODO: fix
		throw new Error(`All sub arrays must be of same length`);

	return Array.from({length: arr[0].length}) 
		.map((_, i) => Array.from({length: arr.length})
			.map((_, k) => arr[k][i]));
}

export function blockComment(c = '-', len = 40, hei = 3) {
	console.log(`${c.repeat(len)}\n`.repeat(hei));
}

export function exper_01 ( arr ) {
	if (arr.length === 0)
		return;

	if (sum(arr.map(el => el.length)) !== arr[0].length * arr.length)
		throw new Error(`All sub arrays must be of same length`);

	return Array.from({length: arr[0].length}) 
		.map((_, i) => Array.from({length: arr.length})
			.map((_, k) => arr[k][i]));
}

/**
 *	PAD
 *	Center a string between padding characters
 *	Curried: true
 *	William Doule
 *	January 21st 2022
 */
export const PAD = n => c => s => s.padStart((n / 2) + ( s.length / 2), c).padEnd(n, c);

//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
export const Colors = () => ({	// colors for use in console
    Reset : "\x1b[0m",
    Bright : "\x1b[1m",
    Dim : "\x1b[2m",
    Underscore : "\x1b[4m",
    Blink : "\x1b[5m",
    Reverse : "\x1b[7m",
    Hidden : "\x1b[8m",

    FgBlack : "\x1b[30m",
    FgRed : "\x1b[31m",
    FgGreen : "\x1b[32m",
    FgYellow : "\x1b[33m",
    FgBlue : "\x1b[34m",
    FgMagenta : "\x1b[35m",
    FgCyan : "\x1b[36m",
    FgWhite : "\x1b[37m",

    BgBlack : "\x1b[40m",
    BgRed : "\x1b[41m",
    BgGreen : "\x1b[42m",
    BgYellow : "\x1b[43m",
    BgBlue : "\x1b[44m",
    BgMagenta : "\x1b[45m",
    BgCyan : "\x1b[46m",
    BgWhite : "\x1b[47m",
});
export const colorSay = color => msg => console.log(`${color}${msg}${Colors.Reset}`);	// log string with color
export const colorError = color => msg => new Error(`${color}${msg}${Colors.Reset}`);	// throw error with colored message
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

// easy to use warper around neo4js acid functions
export const SafeNeo4j = {
    readTransaction: _session => (query, params) => _session.readTransaction(tx => tx.run(query, params)),
    writeTransaction: _session => (query, params) => _session.writeTransaction(tx => tx.run(query, params))
}

