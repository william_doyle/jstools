install:
	npm install https://gitlab.com/william_doyle/jstools

importing:
	import jstools from 'jstools';

CONTRIBUTING:
	-> write test
	-> consider writing a curried and an uncurried version
	-> use other functions in this library where you can
	-> keep everything small and generic
	-> avoid side effects of functions when reasonable
	-> try and keep things const and immutable

Docs:
	read functions.js to understand each function. You can get a list of jstools' function by calling jstools.FindAllMethods(jstools)