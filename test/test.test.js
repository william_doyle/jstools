import {
	/////////CRYPTO//////////////////
	hash,
	ohash,
	GenerateKeys,
	sign,
	checkSignature,
	/////////////////////////////////
	/////////RANDOM//////////////////
	RandomNumber,
	RandomInteger,
	/////////////////////////////////
	/////////FUNCTIONAL PROGRAMMING//
	deepFreeze,
	df,
} from '../index.js';

// either of these import methouds works
//import * as jstools from '../index.js'
import jstools from '../index.js'

const wide_out = true; // set false to disable all output

function allow(sentinal) {
	return wide_out && sentinal;
}

describe('cryptography tests', () => {

	const local_out = !true;

	it('hash() coverage', () => {
		expect(hash('w')).toBe(hash("w"));
		expect(hash('w')).not.toBe(hash("William"));
		expect(hash('w', 'SHA1')).toBe(hash("w", 'SHA1'));
		expect(hash('w', 'SHA1')).not.toBe(hash("w"));
	});

	it('hash() coverage', () => {
		const obj = {
			attribute: 'value',
		};
		
		expect(ohash(obj)).toBe(hash(JSON.stringify(obj)));
	});

	it ('keys, message signing, and signature checking', () => {
		const keys = GenerateKeys(); 
		allow(local_out)&&console.log(JSON.stringify(keys));
		const message = `My name is William and this is a thing I said...`;
		const signature = sign(message, keys.privateKey); 
		const validSignature = checkSignature(message, keys.publicKey, signature); 
		allow(local_out)&&console.log(validSignature);
		expect(validSignature).toBe(true);
	});

});


describe('Randomness tests', () => {
	const local_out = true;

	it('RandomNumber is withing specified bounds', () => {
		const max = 101.121;
		const min = 10.639;
		expect(RandomNumber(min, max) <= max).toBe(true);
		expect(RandomNumber(min, max) >= min).toBe(true);

		allow(local_out)&&console.log(RandomNumber(min, max));
	});
	
	it('RandomInteger is withing specified bounds', () => {
		const max = 101;
		const min = 10;
		expect(RandomInteger(min, max) <= max).toBe(true);
		expect(RandomInteger(min, max) >= min).toBe(true);
		allow(local_out)&&console.log(RandomInteger(min, max));
	});

	it('RandomBool', async () => {
		const val = jstools.RandomBool();
		console.log(val);

	});

	it('random color', async () => {
		const color = jstools.RandomColor();
		expect(color.length).toBe(7);
		const okay_chars = "0123456789abcdef";
		for (let i = 0; i < color.length ; i++)
			expect(okay_chars.includes(color[i]) || (i === 0 && color[i] === '#')).toBe(true);
	});

	it('Random Selection', async () => {
		const choices = ['a', 'b', 'c'];
		const choice = jstools.RandomSelection(choices);
		expect(choices.includes(choice)).toBe(true);
		
		// check for even distribution of selection
		const counts = {a: 0, b: 0, c: 0};
		for (let i = 0; i < 100; i++) {
			const choice = jstools.RandomSelection(choices);
			counts[choice]++;
		}

		// just some arbitrary sanity checks
		expect(jstools.sum([counts.a, counts.b, counts.c])).toBe(100);

		const __allowed_deviation__ = 10; 
		console.log(`standard deviation is ${__allowed_deviation__}`);


		/**
		 *		WARNING... ENABELING THE FOLLOWING TEST WILL RESULT IN OCCASIONAL FASLE POSITINVES.
		 *		IF ENABLED, AND YOUR TEST FAILES, TRY RERUNNING IT ADAIN BEFORE CONCLUDING YOU'RE CODE
		 *		IS TO BLAME. 
		 *
		 *		@important `allow_dangerous_tests` should be set to false!
		 *
		 * */
		const allow_dangerous_tests = false;
		if (allow_dangerous_tests) {
			expect(counts.a <= 33.33 + __allowed_deviation__).toBe(true);
			expect(counts.a >= 33.33 - __allowed_deviation__).toBe(true);

			expect(counts.b <= 33.33 + __allowed_deviation__).toBe(true);
			expect(counts.b >= 33.33 - __allowed_deviation__).toBe(true);

			expect(counts.c <= 33.33 + __allowed_deviation__).toBe(true);
			expect(counts.c >= 33.33 - __allowed_deviation__).toBe(true);
		}
	});


});

describe('file access', () => {
	it('node.read', async () => {
		const s = await jstools.node.read('test.txt');
	});

	it.todo('node.write');
});

describe('functional programming tests', () => {
	const local_out = true;

	it.todo('deepFreeze() coverage');
	it.todo('deepFreeze() and df() are the same');
});

describe('cloning coverage', () => {
	const local_out = true;

	const object_no_class = {
		name: "Bill Zebra",
		description: "Programmer",
		salary: 100,
		paid_in: 'btc',
	};

	it('clone without class', () => {
		const clone = jstools.clone(object_no_class);
		expect(clone).toEqual(object_no_class);
		expect(clone).not.toBe(object_no_class);
		
		const clone2 = jstools.clone_dropClass(object_no_class);
		expect(clone2).toEqual(object_no_class);
		expect(clone2).not.toBe(object_no_class);
		
		expect(clone2).toEqual(clone);
		expect(clone2).not.toBe(clone);
	});

	it.todo('clone withclass');
});

describe ('delta', () => {
	it('delta coverage', () => {
		const a = 1;
		const b = 2;
		const delta = jstools.delta(a)(b);
		expect(delta).toBe(1);
	});
});

describe('vector displacement', () => {

	it('vector displacement makes sense', () => {
		const a = {
			x: 1,
			y: 1,
			z: 0,
		};
		const b = {
			x: 1,
			y: 1,
			z: 0,
		};

		const ans = jstools.VectorDistance(a,b);
		expect(ans).toBe(0);
	});

	it('vector displacement makes sense 2', () => {
		const a = {
			x: 1,
			y: 1,
			z: 0,
		};
		const b = {
			x: 1,
			y: 1,
		};

		const ans = jstools.VectorDistance(a,b);
		expect(ans).toBe(0);
	});

	it('vector a and b can be arrays', () => {

		const a = [1, 1, 1];
		const b = [1, 1, 1];
	
		const ans = jstools.VectorDistance(a,b);
		expect(ans).toBe(0);

		const b2 = [1, 1];
	
		const ans2 = jstools.VectorDistance(a,b2);
		expect(ans2).toBe(1);

	});

	it('vector distance', () => {

		const a = [1, 1, 1, 1, 1];
		const b = [1, 1, 1];
	
		const ans = jstools.VectorDistance(a,b);
		expect(ans).toBe(Math.sqrt(2));
		expect(ans).toBe(jstools.CurriedVectorDistance(a)(b));

	});

});

describe('zip and unzip', () => {
	const letters = ['a', 'b', 'c', 'd',];
	const numbers = [1,    2,	3,	 4,];
	const expectedAfterZip = [['a', 1], ['b', 2], ['c', 3], ['d', 4]];
	
	it('expect zip to work', () => {
		const zipped = jstools.zip(letters)(numbers);
		expect(jstools.SimpleEqual(zipped)(expectedAfterZip)).toBe(true);
	});

	it('expect unzip to work', () => {
		const result = jstools.unzip(expectedAfterZip);
		expect(jstools.SimpleEqual(result[0])(letters)).toBe(true);
		expect(jstools.SimpleEqual(result[1])(numbers)).toBe(true);
	});

	it('zip then unzip', () => {
		const zipped = jstools.zip(letters)(numbers);
		expect(jstools.SimpleEqual(zipped)(expectedAfterZip)).toBe(true);
		const result = jstools.unzip(zipped);
		expect(jstools.SimpleEqual(result[0])(letters)).toBe(true);
		expect(jstools.SimpleEqual(result[1])(numbers)).toBe(true);
	});

	it('unzipN', () => {
		const result = jstools.unzipN(expectedAfterZip);
		expect(jstools.SimpleEqual(result[0])(letters)).toBe(true);
		expect(jstools.SimpleEqual(result[1])(numbers)).toBe(true);
	});

	const shapes = ['triangle', 'square', 'circle', 'pentagon'];
	const lettersNumbersShapes = [
		['a', 1, 'triangle'],
		['b', 2, 'square'],
		['c', 3, 'circle'], 
		['d', 4, 'pentagon']
	];

	it('unzipN with more than 2 element arrays', () => {
		const result = jstools.unzipN(lettersNumbersShapes);
		expect(jstools.SimpleEqual(result[0])(letters)).toBe(true);
		expect(jstools.SimpleEqual(result[1])(numbers)).toBe(true);
		expect(jstools.SimpleEqual(result[2])(shapes)).toBe(true);

		console.log('from', lettersNumbersShapes);
		console.log('to', result);
		const resultresult = jstools.unzipN(result);
		console.log('unzipn again', resultresult );
	});

	it('unzipN play', () => {
		const x_max = 5;
		const y_max = 5;

		///////////////////////////////////////////////////////////////////////////////////////
		// kinda supprised this woks in this order ... leaving it in because its intresting....
		//const manipulate = _manipulate(jstools.unzipN);
		function _manipulate (f) {	// works
//		const _manipulate = f => { // does not work
			return mx => depth =>  {
				if (depth < 1)
					return;
				//const nval = jstools.unzipN(mx);
				const nval = f(mx);
				console.log(nval);
				return manipulate(nval)(--depth);
			}
		}
		const manipulate = _manipulate(jstools.unzipN);
		///////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////
		{
			const mx = Array.from({length: x_max}, () => Array.from({length: y_max}, () => jstools.RandomBit()));
			manipulate(mx)(3);
		}

		{
			const mx = Array.from({length: x_max}, () => Array.from({length: y_max}, () => jstools.RandomHexDigitChar()));
			manipulate(mx)(3);
		}
		
		{
			const mx = Array.from({length: x_max}, (el, i) => Array.from({length: y_max}, (_el, k) => k === i? 'H':' '));
			manipulate(mx)(3);
		}

		{
			const mx = Array.from({length: x_max}, (_, i) => Array.from({length: y_max}, (_, k ) => {
				//if (i === 2 || k ===3 ) 
				if (i === 2 || (i === k && k === 3 )) 
					return 'H';

				return ' ';
			}));
			manipulate(mx)(3);
		}
	});

	it('I wonder what this does', () => {
		jstools.unzipN([]);
	});

});


describe('help coverage', () => {
	it('help can be called', () => {
		jstools.help();
		jstools.help(jstools);
		console.log(jstools.help(jstools));
	});

	it('help always returns a string', () => {
		expect(typeof jstools.help()).toEqual('string');
		expect(typeof jstools.help(jstools)).toEqual('string');
		expect(typeof jstools.help('lol')).toEqual('string');
	});

});

